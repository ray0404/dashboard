/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ttaiit.blogspot.com;

import abstr.Node;
import factory.TreeNodeFactory;
import model.ItemContainer;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Fahad Khan
 */
public class World extends javax.swing.JFrame {
    
    public World(){
        initComponents();
        newnode = null;
        nodemap = new HashMap<>();
        TreeNodeFactory treeNodeFactory = new TreeNodeFactory();
        ItemContainer root = (ItemContainer) treeNodeFactory.getInstance("ItemContainer");
        root.initialize("root",0.0, 0, 0, 0, 0);
        nodemap.put("root", root);
    }          

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfRoot = new javax.swing.JTextField();
        bVisualize = new javax.swing.JButton();
        bAAdRoot = new javax.swing.JButton();
        bAAdChild = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeWorld = new javax.swing.JTree();
        bDelete = new javax.swing.JButton();
        lMessage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        bAAdRoot.setText("Add");
        bAAdRoot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bAAdRootActionPerformed(evt);
            }
        });

        bAAdChild.setText("Modify");
        bAAdChild.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bModifyActionPerformed(evt);
            }
        });

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        treeWorld.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        treeWorld.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                treeWorldMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(treeWorld);

        bDelete.setText("Delete");
        bDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bDeleteActionPerformed(evt);
            }
        });
        
        bVisualize.setText("Visualize");
        bVisualize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	bVisualizeActionPerformed(evt);
            }
        });

        lMessage.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(58, 58, 58)
                            .addComponent(tfRoot, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(bAAdRoot, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(82, 82, 82)
                            .addComponent(bAAdChild)))
                    .addComponent(lMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDelete, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bVisualize, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfRoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(bAAdRoot))
                        .addGap(18, 18, 18)
                        .addComponent(bAAdChild)
                        .addGap(18, 18, 18)
                        .addComponent(bDelete)
                        .addGap(46, 46, 46)
                        .addComponent(bVisualize)
                        .addGap(46, 46, 46)
                        .addComponent(lMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bAAdRootActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAAdRootActionPerformed
        TreePath path = treeWorld.getSelectionPath();
        AddWindow nw = null;
        DefaultMutableTreeNode selectedNode = null;

        lMessage.setText("");
        if (path == null)
            lMessage.setText("Select a node!");
        else {
            selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
            Object itemContainer = nodemap.get(selectedNode.toString());
            String name = itemContainer.getClass().getName();
            if (!selectedNode.toString().equals("root") && nodemap.get(selectedNode.toString()).getClass().getName().equals("model.Item"))
                lMessage.setText("Cannot add element to an item");
            else {
                nw = new AddWindow(this);
                nw.NewScreen();
            }
        }
    }//GEN-LAST:event_bAAdRootActionPerformed

    private void bModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bAAdChildActionPerformed
        ModifyWindow mw = new ModifyWindow(this);
        mw.open();

        
        /*lMessage.setText("");
        DefaultTreeModel model=(DefaultTreeModel) treeWorld.getModel();
        DefaultMutableTreeNode selectedNode=(DefaultMutableTreeNode) treeWorld.getLastSelectedPathComponent();
        DefaultMutableTreeNode newChild=new DefaultMutableTreeNode(tfChild.getText());
        if(!selectedNode.isRoot()){
            lMessage.setText("You Must Select Root or Child");
        }else{
            if(selectedNode!=null){
                if(!tfChild.getText().trim().equals("")){
                    model.insertNodeInto(newChild, selectedNode , selectedNode.getChildCount());
                }else{
                    lMessage.setText("You Must Enter Child Value");
                }
            }else{
                lMessage.setText("You Must Choose a Parent Node to Insert");
            }
        }*/
    }//GEN-LAST:event_bAAdChildActionPerformed
 
    private void bVisualizeActionPerformed(java.awt.event.ActionEvent evt) {
       
        
    } 
 
    private void bDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bDeleteActionPerformed
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treeWorld.getSelectionPath().getLastPathComponent();
        
        
        DefaultTreeModel model = (DefaultTreeModel)treeWorld.getModel();
        model.removeNodeFromParent(selectedNode);      
        
        
        
        /*lMessage.setText("");
        DefaultTreeModel model=(DefaultTreeModel) treeWorld.getModel();
        DefaultMutableTreeNode selectedNode=(DefaultMutableTreeNode) treeWorld.getLastSelectedPathComponent();
        if(selectedNode.isRoot()){
            lMessage.setText("You can't Delete Root");
        }else{
            if(selectedNode!=null){
                model.removeNodeFromParent(selectedNode);
            }else{
                lMessage.setText("You Must Choose a Child to Delete");
            }
            }*/
    }//GEN-LAST:event_bDeleteActionPerformed

    private void treeWorldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_treeWorldMouseClicked
        // TODO add your handling code here:
        TreeSelectionModel smd = treeWorld.getSelectionModel();
        if(smd.getSelectionCount() > 0){
           
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treeWorld.getSelectionPath().getLastPathComponent();
        tfRoot.setText(selectedNode.getUserObject().toString());
    }//GEN-LAST:event_treeWorldMouseClicked
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(World.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(World.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(World.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(World.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new World().setVisible(true);
            }
        });
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAAdChild;
    private javax.swing.JButton bAAdRoot;
    private javax.swing.JButton bDelete;
    private javax.swing.JButton bVisualize;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lMessage;
    private javax.swing.JTextField tfRoot;

    private javax.swing.JTree treeWorld;
    private Map<String, Object> nodemap;
    private Node newnode;

    public Map<String, Object> getNodemap() {
        return nodemap;
    }

    public void setNodemap(Map<String, Object> nodemap) {
        this.nodemap = nodemap;
    }

    public Node getNewnode() {
        return newnode;
    }

    public void setNewnode(Node newnode) {
        this.newnode = newnode;
    }

    public JTree getTreeWorld() {
        return treeWorld;
    }

    public void setTreeWorld(JTree treeWorld) {
        this.treeWorld = treeWorld;
    }
// End of variables declaration//GEN-END:variables
}
//end